package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SecondEffectDecorator implements IPumpkin {

    IPumpkin pumpkin;
    @Override
    public String description() {
        return pumpkin.description()+" + E2";
    }

    @Override
    public int growth(int light) {
        return pumpkin.growth(light)/2;
    }
}
