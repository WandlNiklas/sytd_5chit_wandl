package at.htl;

import java.util.List;

public class FourthEffectApply implements IApply {
    @Override
    public IPumpkin apply(int day, IPumpkin pumpkin, List<Integer> inputList) {
        if(100-inputList.get(day)>=50)
            pumpkin = new FourthEffectDecorator(pumpkin);
        return pumpkin;
    }
}
