package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pumpkin implements IPumpkin {
    int defaultLight;
    @Override
    public String description() {
        return "";
    }

    @Override
    public int growth(int light) {
        return defaultLight;
    }
}
