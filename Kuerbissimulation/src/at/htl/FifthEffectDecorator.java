package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FifthEffectDecorator implements IPumpkin {

    IPumpkin pumpkin;
    @Override
    public String description() {
        return pumpkin.description()+" + E5";
    }

    @Override
    public int growth(int light) {
        return pumpkin.growth(light)-1;
    }
}
