package at.htl;

import java.util.List;

public class SecondEffectApply implements IApply {
    @Override
    public IPumpkin apply(int day, IPumpkin pumpkin, List<Integer> inputList) {
        if(day>=4)
        {
            int water = 0;
            for(int i = 0; i < 5; i++)
            {
                water+=100-inputList.get(day-i);
            }
            if(water<10)
                pumpkin = new SecondEffectDecorator(pumpkin);
        }
        return pumpkin;
    }
}
