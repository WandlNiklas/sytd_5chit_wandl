package at.htl;

import java.util.List;

public class FifthEffectApply implements IApply {
    @Override
    public IPumpkin apply(int day, IPumpkin pumpkin, List<Integer> inputList) {
        if(day>=1)
        {
            int water = 0;
            for(int i = 0; i < 2; i++)
            {
                water+=100-inputList.get(day-i);
            }
            if(water<=30)
                pumpkin = new FifthEffectDecorator(pumpkin);
        }
        return pumpkin;
    }
}
