package at.htl;

import java.util.List;

public interface IApply {
    public IPumpkin apply(int day, IPumpkin pumpkin, List<Integer> inputList);
}
