package at.htl;

import java.util.List;

public class ThirdEffectApply implements IApply {
    @Override
    public IPumpkin apply(int day, IPumpkin pumpkin, List<Integer> inputList) {
        if(day>=9)
        {
            int water = 0;
            for(int i = 0; i < 10; i++)
            {
                water+=100-inputList.get(day-i);
            }
            if(water<10)
                pumpkin = new ThirdEffectDecorator(pumpkin);
        }
        return pumpkin;
    }
}
