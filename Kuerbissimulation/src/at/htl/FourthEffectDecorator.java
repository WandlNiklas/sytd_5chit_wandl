package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FourthEffectDecorator implements IPumpkin {

    IPumpkin pumpkin;
    @Override
    public String description() {
        return pumpkin.description()+" + E4";
    }

    @Override
    public int growth(int light) {
        return pumpkin.growth(light)-1;
    }
}
