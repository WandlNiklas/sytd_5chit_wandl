package at.htl;

import lombok.AllArgsConstructor;
@AllArgsConstructor
public class FirstEffectDecorator implements IPumpkin {

    IPumpkin pumpkin;
    @Override
    public String description() {
        return pumpkin.description()+"E1";
    }

    @Override
    public int growth(int light) {
        return light/20;
    }
}
