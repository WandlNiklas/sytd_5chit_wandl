package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor

public class ThirdEffectDecorator implements IPumpkin {

    IPumpkin pumpkin;
    @Override
    public String description() {
        return pumpkin.description()+" + E3";
    }

    @Override
    public int growth(int light) {
        return 0;
    }
}
