package at.htl;

import java.util.List;

public class FirstEffectApply implements IApply {
    @Override
    public IPumpkin apply(int day, IPumpkin pumpkin, List<Integer> inputList) {
        pumpkin = new FirstEffectDecorator(pumpkin);
        return pumpkin;
    }
}
