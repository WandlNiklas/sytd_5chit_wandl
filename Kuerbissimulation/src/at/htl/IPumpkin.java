package at.htl;

public interface IPumpkin {
    String description();
    int growth(int light);
}
