package at.htl;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> lightList = new ArrayList<>();
        lightList.add(65);
        lightList.add(55);
        lightList.add(78);
        lightList.add(72);
        lightList.add(97);
        lightList.add(99);
        lightList.add(99);
        lightList.add(99);
        lightList.add(100);
        lightList.add(75);
        lightList.add(10);
        lightList.add(12);
        lightList.add(10);
        lightList.add(9);
        lightList.add(10);
        PumpkinSimulator pumpkinSimulator = new PumpkinSimulator();
        System.out.println(pumpkinSimulator.simulate(lightList));

    }
}

