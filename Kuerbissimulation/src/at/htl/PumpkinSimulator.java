package at.htl;

import java.util.ArrayList;
import java.util.List;

public class PumpkinSimulator {
    public List<String> simulate(List<Integer> lightList) {

        List<String> output = new ArrayList<>();

        for (int i = 0; i < lightList.size(); i++) {

            IPumpkin pumpkin = new Pumpkin(0);

            pumpkin = new FirstEffectApply().apply(i,pumpkin,lightList);

            pumpkin = new SecondEffectApply().apply(i,pumpkin,lightList);

            pumpkin = new ThirdEffectApply().apply(i,pumpkin,lightList);

            pumpkin = new FourthEffectApply().apply(i,pumpkin,lightList);

            pumpkin = new FifthEffectApply().apply(i,pumpkin,lightList);

            output.add("Tag "+i+": "+pumpkin.growth(lightList.get(i))+"% "+pumpkin.description()+"\n");
        }
        return output;
    }
}
