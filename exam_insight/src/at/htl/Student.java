package at.htl;

import java.util.concurrent.Semaphore;

public class Student implements Runnable {
    Semaphore semS, semA, semE;

    public Student(Semaphore semS, Semaphore semE) {
        this.semS = semS;
        this.semE = semE;
    }

    @Override
    public void run() {
        try {
            while (true) {
                semE.release();
                semS.acquire();
                insight();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void insight() {
        System.out.println("Insight S");
    }
}
