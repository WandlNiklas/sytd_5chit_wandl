package at.htl;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {
        Semaphore semS = new Semaphore(0);
        Semaphore semT = new Semaphore(1);
        Semaphore semE = new Semaphore(0);

        Thread student = new Thread(new Student(semS, semE));

        Thread assistent1 = new Thread(new Assistent(semS, semT, semE));
        Thread assistent2 = new Thread(new Assistent(semS, semT, semE));
        Thread assistent3 = new Thread(new Assistent(semS, semT, semE));
        Thread assistent4 = new Thread(new Assistent(semS, semT, semE));
        Thread assistent5 = new Thread(new Assistent(semS, semT, semE));

        assistent1.start();
        assistent2.start();
        assistent3.start();
        assistent4.start();
        assistent5.start();
        student.start();
    }
}
