package at.htl;

import java.util.concurrent.Semaphore;

public class Assistent implements Runnable {

    Semaphore semA, semS, semT, semE;

    public Assistent(Semaphore semS, Semaphore semT, Semaphore semE) {
        this.semS = semS;
        this.semT = semT;
        this.semE = semE;
    }

    @Override
    public void run() {
        try {
            while (true) {
                semE.acquire();
                semT.acquire();
                fetchExam();
                semT.release();
                semS.release();
                insight();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void fetchExam() {
        System.out.println("Taking exam");
    }

    void insight() {
        System.out.println("Insight A");
    }
}
