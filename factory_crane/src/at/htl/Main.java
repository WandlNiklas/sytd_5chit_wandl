package at.htl;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {
        Semaphore semA = new Semaphore(0);
        Semaphore semB = new Semaphore(0);
        Semaphore semConveyorBelt = new Semaphore(0);

        Thread threadMachineB = new Thread(new MachineB(semB, semConveyorBelt));
        Thread threadMachineA = new Thread(new MachineA(semA, semConveyorBelt));
        Thread threadCrane = new Thread(new Crane(semA, semB, semConveyorBelt));

        threadCrane.start();
        threadMachineA.start();
        threadMachineB.start();
    }
}
