package at.htl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {

    // 1. Lambda Function
    // Eine Lambda Function ist ein Programmartefakt wie ein Objekt. Sie kann referenziert werden.

    // (Dish d) -> {return d.getName();}    //Lambda Funktion
    // (Dish d) -> d.getName()              //Lambda Expression
    // (Dish d) -> Dish::getName()          //Lambda Short

    // 2. Verwendung von Lambda Funktionen
    // Überall wo ein Funktionales Interface als Typ verwendet wird, kann eine Lambda Funktion verwendet werden.

    // 3. Funktionales Interface
    // 1) Annotation @FunctionalInterface
    // 2) Nur eine Methodensignatur wird definiert

    // 4. Systemdefined Functional Interfaces
    // a) Predicate:    test(T -> boolean)
    // b) Consumer:     accept(T -> void)
    // c) Function:     apply(T -> R)
    // d) Runnable:     run(() -> void)

    // 5. Stream API
    // Die Stream API gibt dem Programmierer die Möglichkeit Collections von Objekten parallel und deklarativ zu verarbeiten
    public static void main(String[] args) {

        List<Dish> dishes = Arrays.asList(
                new Dish("Schniener Witzel", Dish.EDishType.MEAT, 15),
                new Dish("Werner Bürstel", Dish.EDishType.MEAT, 10),
                new Dish("Vorona Cirus", Dish.EDishType.OTHER, 0),
                new Dish("Hetznfua", Dish.EDishType.FISH, 3)
        );

        List<Dish> excellentFood = new ArrayList<>();
        for(Dish d: dishes)
        {
            if(d.getType()== Dish.EDishType.MEAT)
                excellentFood.add(d);
        }
        List<String> dishNames = new ArrayList<>();
        for(Dish d:excellentFood)
        {
            dishNames.add(d.getName());
        }
        for(String s: dishNames)
        {
            System.out.println(s);
        }
        dishes.parallelStream().filter((Dish d)->d.getType()== Dish.EDishType.MEAT).map((Dish d)->d.getName()).forEach(System.out::println);
        /*Consumer<Apple> x;
        Function<Apple, String> y;
        Runnable r;

        Predicate<Apple> predicate = (Apple a) -> {
            return a.getTaste() == Apple.Taste.SWEET;
        };
        Predicate<Apple> predicate2 = (Apple a) -> a.getTaste()==Apple.Taste.SWEET;

        AppleDealer julian = new AppleDealer();
        List<Apple> pickedApples = julian.pick(Arrays.asList(
                new Apple("Pink Lady", Apple.Color.RED, 3, Apple.Taste.SWEET),
                new Apple("Gouda", Apple.Color.YELLOW, 10, Apple.Taste.SOUR),
                new Apple("Kronprinz Rudolf", Apple.Color.RED, 7, Apple.Taste.SWEET),
                new Apple("Lehrbua", Apple.Color.RED, 11, Apple.Taste.SOUR)),
                predicate2);
        for (Apple a:pickedApples
             ) {
            System.out.println(a.toString());
        }
        List<Dish> dishes = julian.pick(Arrays.asList(
                new Dish("Schniener Witzel", Dish.EDishType.MEAT, 15),
                new Dish("Werner Bürstel", Dish.EDishType.MEAT, 10),
                new Dish("Vorona Cirus", Dish.EDishType.OTHER, 0),
                new Dish("Hetznfua", Dish.EDishType.FISH, 3)
        ),(Dish d)->{return d.getType()==Dish.EDishType.OTHER;});
        for(Dish d: dishes)
        {System.out.println(d.toString());}*/
    }
}
