package at.htl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class AppleDealer {

    public <T> List<T> pick(List<T> apples, Predicate<T> p) {
        List<T> pickedApples = new ArrayList<>();

        for (T t : apples) {
            if (p.test(t)) {
                pickedApples.add(t);
            }
        }
        return pickedApples;
    }
}
