package at.htl;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Dimension implements Serializable {
    private int width;
    private int height;
    private int depth;
}
