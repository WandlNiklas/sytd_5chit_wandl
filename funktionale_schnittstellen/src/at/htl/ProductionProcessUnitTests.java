package at.htl;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ProductionProcessUnitTests {
    List<Ware> myItems = Arrays.asList(
            new Ware("Corona Beer", ECondition.NEW, 21, new Dimension(1, 5, 1)),
            new Ware("Headset", ECondition.USED, 25, new Dimension(5, 3, 3)),
            new Ware("Coke", ECondition.NEW, 1, new Dimension(2, 5, 2)),
            new Ware("Sebastian Ferchenbauer", ECondition.DAMAGED, 85, new Dimension(40, 190, 30)) //eine tolle Ware, ich weiß
    );
    Predicate<Ware> newPredicate = (Ware w) -> w.getCondition() == ECondition.NEW;
    Predicate<Ware> lightPredicate = (Ware w) -> w.getWeight() < 20;
    ProductionProcess pp = new ProductionProcess();

    @Test
    public void newOnly() {
        List<Ware> filteredItems = pp.evaluate(myItems, newPredicate);
        Assert.assertEquals(2,filteredItems.size());
    }

    @Test
    public void newAndLight() {
        List<Ware> filteredItems = pp.evaluate(myItems, newPredicate.and(lightPredicate));
        Assert.assertEquals(1,filteredItems.size());
    }
}
