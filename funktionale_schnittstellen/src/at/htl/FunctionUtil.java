package at.htl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class FunctionUtil {
    public List<Integer> evaluateFunction(List<Integer> values, Function<Integer, Integer> f) {
        List<Integer> result = new ArrayList<>();
        for (Integer i : values) {
            result.add(f.apply(i));
        }
        return result;
    }
}
