package at.htl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class ProductionProcess {
    public List<Ware> evaluate(List<Ware> wares, Predicate<Ware> predicate) {
        List<Ware> pickedWare = new ArrayList<>();
        for (Ware w : wares) {
            if (predicate.test(w)) {
                pickedWare.add(w);
            }
        }
        return pickedWare;
    }
}
