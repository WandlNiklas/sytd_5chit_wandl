package at.htl;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class FunctionUtilUnitTests {
    FunctionUtil fu = new FunctionUtil();
    List<Integer> xValues = Arrays.asList(0,1,2,3,4,5,6,7,8,9);
    Function<Integer, Integer> xAddOne = (Integer i)->i+1;
    Function<Integer, Integer> xMultiplyByTwo = (Integer i)->i*2;

    @Test
    public void AddOne()
    {
        List<Integer> shouldResult = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            shouldResult.add(i+1);
        }
        List<Integer> result = fu.evaluateFunction(xValues,xAddOne);
        Assert.assertEquals(shouldResult,result);
    }
    @Test
    public void MultiplyByTwo()
    {
        List<Integer> shouldResult = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            shouldResult.add(i*2);
        }
        List<Integer> result = fu.evaluateFunction(xValues,xMultiplyByTwo);
        Assert.assertEquals(shouldResult,result);
    }
    @Test
    public void Combined()
    {
        List<Integer> shouldResult = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            shouldResult.add(i*2+1);
        }
        List<Integer> result = fu.evaluateFunction(xValues,xMultiplyByTwo.andThen(xAddOne));
        Assert.assertEquals(shouldResult,result);
    }
}
