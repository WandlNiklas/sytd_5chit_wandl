package at.htl;

public class EspressoFactory implements IIngredientFactory{
    @Override
    public IIngredient createInstance()
    {
        return new Espresso();
    }
}
