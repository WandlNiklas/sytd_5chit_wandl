package at.htl;

public interface IIngredient {
    float price();
    String description();
}
