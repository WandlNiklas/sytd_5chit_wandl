package at.htl;

public class DecaffeinatedFactory implements IIngredientFactory{
    @Override
    public IIngredient createInstance()
    {
        return new SoyDecorator(new Decaffeinated());
    }
}
