package at.htl;

public class HouseMixFactory implements IIngredientFactory{
    @Override
    public IIngredient createInstance()
    {
        return new MilkFoamDecorator(new ChocolateDecorator(new SoyDecorator((new HouseMix()))));
    }
}
