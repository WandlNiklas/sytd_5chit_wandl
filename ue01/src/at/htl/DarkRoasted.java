package at.htl;

public class DarkRoasted implements IIngredient {
    @Override
    public String description()
    {
        return "Dark Roasted";
    }
    @Override
    public float price(){
        return 3f;
    }
}
