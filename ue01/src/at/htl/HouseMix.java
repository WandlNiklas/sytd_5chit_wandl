package at.htl;

public class HouseMix implements IIngredient {
    @Override
    public String description()
    {
        return "HouseMix";
    }
    @Override
    public float price(){
        return 2.5f;
    }
}
