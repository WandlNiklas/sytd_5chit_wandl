package at.htl;

public class Decaffeinated implements IIngredient {
    @Override
    public String description()
    {
        return "Decaffeinated";
    }
    @Override
    public float price(){
        return 2.75f;
    }
}
