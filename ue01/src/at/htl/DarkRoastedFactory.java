package at.htl;

public class DarkRoastedFactory implements IIngredientFactory{
    @Override
    public IIngredient createInstance()
    {
        return new MilkDecorator(new SoyDecorator(new DarkRoasted()));
    }
}
