package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SoyDecorator implements IIngredient {

    IIngredient coffee;

    @Override
    public float price(){
        return coffee.price()+0.75f;
    }
    @Override
    public String description(){
        return coffee.description()+" + soy";
    }
}
