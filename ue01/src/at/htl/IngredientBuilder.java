package at.htl;

public class IngredientBuilder {
    private IIngredient ingredient;

    public IngredientBuilder init(IIngredient ingredient)
    {
        this.ingredient = ingredient;
        return this;
    }
    public IIngredient create()
    {
        return ingredient;
    }
}
