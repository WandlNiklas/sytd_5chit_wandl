package at.htl;

public interface IIngredientFactory {
    IIngredient createInstance();
}
