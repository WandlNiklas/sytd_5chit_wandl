package at.htl;

import java.io.Serializable;

public class CoffeeHouseSimulator implements Serializable {
    public void ausgabe(IIngredient ingredient)
    {
        System.out.println(ingredient.description()+" - "+ingredient.price()+"€");
    }
}
