package at.htl;

import sun.security.util.ECKeySizeParameterSpec;

public class Main {

    public static void main(String[] args) {
        IIngredient darkRoasted = new MilkDecorator(new SoyDecorator(new DarkRoasted()));
		IIngredient houseMix = new MilkFoamDecorator(new ChocolateDecorator(new SoyDecorator((new HouseMix()))));
		IIngredient decaffeinated = new SoyDecorator(new Decaffeinated());
		IIngredient espresso = new Espresso();

	CoffeeHouseSimulator coffeeHouseSimulator = new CoffeeHouseSimulator();
	coffeeHouseSimulator.ausgabe(darkRoasted);
	coffeeHouseSimulator.ausgabe(houseMix);
	coffeeHouseSimulator.ausgabe(decaffeinated);
	coffeeHouseSimulator.ausgabe(espresso);

	//FACTORY

	DarkRoastedFactory drf = new DarkRoastedFactory();
	HouseMixFactory hmf = new HouseMixFactory();
	DecaffeinatedFactory df = new DecaffeinatedFactory();
	EspressoFactory ef = new EspressoFactory();

	IIngredient darkRoastedf = drf.createInstance();
	IIngredient houseMixf = hmf.createInstance();
	IIngredient decaffeinatedf = df.createInstance();
	IIngredient espressof = ef.createInstance();

	coffeeHouseSimulator.ausgabe(darkRoastedf);
	coffeeHouseSimulator.ausgabe(houseMixf);
	coffeeHouseSimulator.ausgabe(decaffeinatedf);
	coffeeHouseSimulator.ausgabe(espressof);

	//BUILDER

	IngredientBuilder ib = new IngredientBuilder();

	IIngredient darkRoastedb = ib.init(new MilkDecorator(new SoyDecorator(new DarkRoasted()))).create();
	System.out.println(darkRoastedb.description()+" - "+darkRoastedb.price());
	IIngredient houseMixb = ib.init(new MilkFoamDecorator(new ChocolateDecorator(new SoyDecorator((new HouseMix()))))).create();
	System.out.println(houseMixb.description()+" - "+houseMixb.price());
	IIngredient decaffeinatedb = ib.init(new SoyDecorator(new Decaffeinated())).create();
	System.out.println(decaffeinatedb.description()+" - "+decaffeinatedb.price());
	IIngredient espressob = ib.init(new Espresso()).create();
	System.out.println(espressob.description()+" - "+espressob.price());
    }
}
