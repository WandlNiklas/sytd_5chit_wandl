package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MilkDecorator implements IIngredient {

    IIngredient coffee;

    @Override
    public float price(){
        return coffee.price()+0.5f;
    }
    @Override
    public String description(){
        return coffee.description()+" + milk";
    }
}
