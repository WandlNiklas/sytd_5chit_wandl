package at.htl;

public class Espresso implements IIngredient {
    @Override
    public String description()
    {
        return "Espresso";
    }
    @Override
    public float price(){
        return 2.25f;
    }
}
