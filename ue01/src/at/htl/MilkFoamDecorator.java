package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MilkFoamDecorator implements IIngredient {

    IIngredient coffee;

    @Override
    public float price(){
        return coffee.price()+0.25f;
    }
    @Override
    public String description(){
        return coffee.description()+" + milk foam";
    }
}
