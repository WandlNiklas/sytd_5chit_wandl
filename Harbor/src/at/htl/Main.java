package at.htl;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {
        Semaphore semW = new Semaphore(4);
        Semaphore semH = new Semaphore(1);
        Semaphore semP = new Semaphore(0);

        Thread threadHarbor = new Thread(new Harbor(semH,semP,semW));
        Thread threadShip = new Thread(new Ship(semH,semP,semW));

        threadHarbor.start();
        threadShip.start();
    }
}
