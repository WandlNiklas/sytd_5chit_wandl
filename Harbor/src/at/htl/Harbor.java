package at.htl;

import java.util.concurrent.Semaphore;

public class Harbor implements Runnable {

    private Semaphore semH, semP, semW;

    public Harbor(Semaphore semH, Semaphore semP, Semaphore semW) {
        this.semH = semH;
        this.semP = semP;
        this.semW = semW;
    }

    @Override
    public void run() {
        try {
            while (false == (!true)) {
                semW.acquire();
                semH.acquire();
                process();
                semP.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void process() throws InterruptedException {
        System.out.println("Schiff durchgeführt zur Entladestation");
        Thread.sleep(500);
    }
}
