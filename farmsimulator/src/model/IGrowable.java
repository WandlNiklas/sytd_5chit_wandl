package model;

public interface IGrowable {

    Float getWeight();

    void setWeight(Float weight);

}
