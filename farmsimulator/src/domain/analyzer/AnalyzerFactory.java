package domain.analyzer;

public class AnalyzerFactory implements IAnalyerFactory{
    @Override
    public IAnalyzer createAnalyzer() {
        return new DroughtAnalyzer(new SnailInfestationAnalyzer(new SnailIncidenceAnalyzer(new AridityAnalyzer(new DaylightRatioAnalyzer()))));
    }
}
