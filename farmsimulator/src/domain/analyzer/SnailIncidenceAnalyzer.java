package domain.analyzer;

import domain.core.EnvironmentRecord;
import domain.core.GrowthRateRecord;
import domain.core.RecordList;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SnailIncidenceAnalyzer implements IAnalyzer {

    @NonNull
    private IAnalyzer analyzer;

    @Override
    public GrowthRateRecord calculateGrowthRatio(RecordList<EnvironmentRecord> daylightRatioList, EnvironmentRecord record) {
        GrowthRateRecord growthRateRecord = analyzer.calculateGrowthRatio(daylightRatioList, record);

        if (record.getRainRatio() >= 50) {
            growthRateRecord.setGrowthRate(growthRateRecord.getGrowthRate() - 1);
            growthRateRecord.addEffectDescription(getDescription());
        }

        return growthRateRecord;
    }

    @Override
    public String getDescription() {
        return "Effect 4: Snail incidence";
    }
}
