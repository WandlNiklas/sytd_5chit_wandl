package domain.analyzer;

public interface IAnalyerFactory {

    IAnalyzer createAnalyzer();

}
