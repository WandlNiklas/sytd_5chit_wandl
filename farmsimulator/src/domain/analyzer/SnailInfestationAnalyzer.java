package domain.analyzer;

import domain.core.EnvironmentRecord;
import domain.core.GrowthRateRecord;
import domain.core.RecordList;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class SnailInfestationAnalyzer implements IAnalyzer{

    @NonNull
    private IAnalyzer analyzer;

    @Override
    public GrowthRateRecord calculateGrowthRatio(RecordList<EnvironmentRecord> daylightRatioList, EnvironmentRecord record) {
        GrowthRateRecord growthRateRecord = analyzer.calculateGrowthRatio(daylightRatioList, record);

        List<EnvironmentRecord> subRecordList = daylightRatioList.precedingRecordSublist(record, 2);

        if(subRecordList.size() == 2){
            int rainAmountInLast2Days = subRecordList.stream().map(EnvironmentRecord::getRainRatio).reduce(0, (a, b) -> a+b);

            if(rainAmountInLast2Days >= 30){
                growthRateRecord.setGrowthRate(growthRateRecord.getGrowthRate() - 1);
                growthRateRecord.addEffectDescription(getDescription());
            }
        }

        return growthRateRecord;
    }

    @Override
    public String getDescription() {
        return "Effect 5: Snail Infestation";
    }

}
