package domain.analyzer;

import domain.core.EnvironmentRecord;
import domain.core.GrowthRateRecord;
import domain.core.RecordList;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class DroughtAnalyzer implements IAnalyzer{

    @NonNull
    private IAnalyzer analyzer;

    @Override
    public GrowthRateRecord calculateGrowthRatio(RecordList<EnvironmentRecord> daylightRatioList, EnvironmentRecord record) {
        GrowthRateRecord growthRateRecord = analyzer.calculateGrowthRatio(daylightRatioList, record);
        int growthAmount = growthRateRecord.getGrowthRate();

        List<EnvironmentRecord> subRecordList = daylightRatioList.precedingRecordSublist(record, 10);

        if(subRecordList.size() == 10){
            int rainAmountInLast10Days = subRecordList.stream().map(EnvironmentRecord::getRainRatio).reduce(0, (a, b) -> a+b);

            if(rainAmountInLast10Days < 10){
                growthAmount = 0;

                growthRateRecord.setGrowthRate(growthAmount);
                growthRateRecord.addEffectDescription(getDescription());
            }
        }

        return growthRateRecord;
    }

    @Override
    public String getDescription() {
        return "Effect 3: Growth halted for drought period";
    }
}
