package domain.analyzer;

import domain.core.EnvironmentRecord;
import domain.core.GrowthRateRecord;
import domain.core.RecordList;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class AridityAnalyzer implements IAnalyzer {

    @NonNull
    private IAnalyzer analyzer;

    @Override
    public GrowthRateRecord calculateGrowthRatio(RecordList<EnvironmentRecord> daylightRatioList, EnvironmentRecord record) {
        GrowthRateRecord growthRecord = analyzer.calculateGrowthRatio(daylightRatioList, record);
        int growthAmount = growthRecord.getGrowthRate();

        List<EnvironmentRecord> subRecordList = daylightRatioList.precedingRecordSublist(record, 5);

        if(subRecordList.size() == 5){
            int rainAmountInLast5Days = subRecordList.stream().map(EnvironmentRecord::getRainRatio).reduce(0, (a, b) -> a+b);

            if(rainAmountInLast5Days < 10){
                growthAmount /= 2;

                growthRecord.setGrowthRate(growthAmount);
                growthRecord.addEffectDescription(getDescription());
            }
        }

        return growthRecord;
    }

    @Override
    public String getDescription() {
        return "Effect 2: Little rain in last 5 days";
    }

}
