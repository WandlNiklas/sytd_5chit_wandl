package domain.core;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

public class EnvironmentRecord implements Serializable{

    @Getter
    private Integer daylightRatio;

    public EnvironmentRecord(Integer daylightRatio){
        if(daylightRatio < 0 || daylightRatio > 100)
            throw new InvalidRecordException();

        this.daylightRatio = daylightRatio;
    }

    public Integer getRainRatio(){
        return 100 - this.daylightRatio;
    }

}
