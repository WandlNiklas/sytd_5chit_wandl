package domain.core;

import domain.analyzer.AnalyzerFactory;
import domain.analyzer.IAnalyzer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GrowSimulator {


    private RecordList<EnvironmentRecord> inputData = new RecordList<>();

    private List<GrowthRateRecord> growthRateData = new ArrayList<>();

    private IAnalyzer analyzer;

    public GrowSimulator() {
        analyzer = new AnalyzerFactory().createAnalyzer();
    }

    public void init(Float initialWeight) {
        inputData = new RecordList<>();
        growthRateData = new ArrayList<>();
    }

    public void addInputRecord(int lightRatio) {
        inputData.add(new EnvironmentRecord(lightRatio));
    }

    public void runSimulation() {
        analyzeData();
        simulateOutcome();
    }

    private void analyzeData() {
        inputData.forEach((EnvironmentRecord record) -> {
            growthRateData.add(this.analyzer.calculateGrowthRatio(inputData, record));
        });
    }

    private void simulateOutcome() {
        growthRateData.forEach((GrowthRateRecord record) -> System.out.println(record.toString()));
    }

}
