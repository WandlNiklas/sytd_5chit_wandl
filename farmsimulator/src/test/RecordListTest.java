package test;

import domain.core.RecordList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

public class RecordListTest {

    @Test
    public void indexTest(){
        RecordList<Integer> list = new RecordList<>();

        Integer record1 = new Integer(5);
        Integer record2 = new Integer(4);
        Integer record3 = new Integer(3);
        Integer record4 = new Integer(4);
        Integer record5 = new Integer(2);

        list.add(record1);
        list.add(record2);
        list.add(record3);
        list.add(record4);
        list.add(record5);

        List<Integer> sublist = list.precedingRecordSublist(record3, 5);
        assertEquals(sublist.size(), 3);
        assertTrue(sublist.containsAll(Arrays.asList(record1, record2, record3)));

        sublist = list.precedingRecordSublist(record3, 3);
        assertEquals(sublist.size(), 3);
        assertTrue(sublist.containsAll(Arrays.asList(record1, record2, record3)));

        sublist = list.precedingRecordSublist(record3, 2);
        assertEquals(sublist.size(), 2);
        assertTrue(sublist.containsAll(Arrays.asList(record2, record3)));

    }

}
