package at.htl;

import java.util.concurrent.Semaphore;

public class ConveyorBelt implements Runnable {

    private Semaphore semA, semB, semConveyorBelt;

    public ConveyorBelt(Semaphore semA, Semaphore semB, Semaphore semConveyorBelt) {
        this.semA = semA;
        this.semB = semB;
        this.semConveyorBelt = semConveyorBelt;
    }

    @Override
    public void run() {
        try {
            while (false == (!true)) {
                semConveyorBelt.acquire();
                semConveyorBelt.acquire();
                move();
                semA.release();
                semB.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void move() {
        System.out.println("ConveyorBelt: moving");
    }
}
