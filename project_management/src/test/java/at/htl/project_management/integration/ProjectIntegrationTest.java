package at.htl.project_management.integration;

import at.htl.project_management.message.producer.ProjectProducer;
import at.htl.project_management.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class ProjectIntegrationTest {
    @Autowired
    private ProjectProducer producer;

    @Test
    public void sendEmployeeTest() throws JsonProcessingException {

        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","message","project/approvement/init");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        ManagementProject testProject = new ManagementProject();
        testProject.setLawType(ELawType.P_27);
        testProject.setProjectState(EProjectState.CREATED);
        testProject.setDescription("Test Project Description");
        Facility facility = new Facility();
        facility.setFacilityName("science");
        testProject.setFacility(facility);
        testProject.setTitle("TestProject");

        HttpEntity<AProject> requestData = new HttpEntity<>(testProject, httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.postForEntity(requestURL, requestData, Void.class);
    }
}
