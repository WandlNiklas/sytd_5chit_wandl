package at.htl.project_management.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RequestFundingProject extends AProject {
    private Boolean isFFWFunded;
    private Boolean isFFGFunded;
    private Boolean isEUFunded;
}

