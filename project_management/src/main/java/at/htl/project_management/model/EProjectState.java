package at.htl.project_management.model;

public enum EProjectState {
    CREATED, IN_APPROVEMENT, CANCELLED, APPROVED
}
