package at.htl.project_management.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ManagementProject extends AProject {
    private ELawType lawType;
}
