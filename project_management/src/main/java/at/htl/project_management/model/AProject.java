package at.htl.project_management.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json-type") //json neues Attribut
@JsonSubTypes({
        @JsonSubTypes.Type(value = ManagementProject.class, name = "managementProject"),
        @JsonSubTypes.Type(value = RequestFundingProject.class, name = "requestFundingProject")})
@Data
@ToString
public abstract class AProject implements Serializable {
    private Facility facility;

    private String title;

    private String description;

    private EProjectState projectState;
}
