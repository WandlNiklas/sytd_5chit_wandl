package at.htl.project_management.message.producer;

import at.htl.project_management.model.AProject;
import at.htl.project_management.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectResultProducer {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendProjectResult(AProject project) throws JsonProcessingException {
        String queueName = "";
        if(project.getProjectState() == EProjectState.APPROVED)
            queueName = "accepted";
        if(project.getProjectState() == EProjectState.CANCELLED)
            queueName = "cancelled";

        String message = objectMapper.writeValueAsString(project);

        rabbitTemplate.convertAndSend("htl.project.resultExchange",queueName,message);
    }
}
