package at.htl.project_management.message.consumer;

import at.htl.project_management.message.producer.ProjectResultProducer;
import at.htl.project_management.model.AProject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectResultConsumer {
    private static final Logger logger = LoggerFactory.getLogger(ProjectResultConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @RabbitListener(queues = {"htl.project.resultAccepted","htl.project.resultCancelled"})
    public void listenProjectResultAccepted(String message) throws JsonProcessingException {
        AProject project = objectMapper.readValue(message, AProject.class);
        logger.info("Received Project: {}",project.getTitle());
    }
}
