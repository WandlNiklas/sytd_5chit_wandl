package at.htl.project_management.message.consumer;

import at.htl.project_management.message.producer.ProjectResultProducer;
import at.htl.project_management.model.AProject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectConsumer {
    private static final Logger logger = LoggerFactory.getLogger(ProjectConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ProjectResultProducer producer;

    @RabbitListener(queues = {"htl.project.science","htl.project.architecture","htl.project.electricity"})
    public void listenScienceProject(String message) throws JsonProcessingException {
        AProject project = objectMapper.readValue(message, AProject.class);
        logger.info("Received Project: {}",project.getTitle());

        producer.sendProjectResult(project);
    }
}
