package at.htl.project_management.message.producer;

import at.htl.project_management.model.AProject;
import at.htl.project_management.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;
@Component
public class ProjectProducer {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendProject(AProject project) throws JsonProcessingException {
        Random r = new Random();
        if(r.nextInt(6) == 1)
            project.setProjectState(EProjectState.CANCELLED);
        else
            project.setProjectState(EProjectState.APPROVED);
        String message = objectMapper.writeValueAsString(project);

        rabbitTemplate.convertAndSend("htl.project.exchange",project.getFacility().getFacilityName(),message);
    }
}
