package at.htl.project_management.service;

import at.htl.project_management.message.producer.ProjectProducer;
import at.htl.project_management.model.AProject;
import at.htl.project_management.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/project")
public class ProjectResource {
    @Autowired
    private ProjectProducer projectProducer;

    @PostMapping(path="/approvement/init")
    @ResponseStatus(HttpStatus.OK)
    public void approveProject(@RequestBody AProject project) throws JsonProcessingException {
        if(project.getProjectState() == EProjectState.CREATED) {
            projectProducer.sendProject(project);
        } else {
            //400 Bad Request
        }
    }
}
