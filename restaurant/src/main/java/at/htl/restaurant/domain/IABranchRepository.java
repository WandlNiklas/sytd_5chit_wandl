package at.htl.restaurant.domain;

import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Table;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IABranchRepository extends JpaRepository<ABranch,Long> {
    List<ABranch> getABranchByNameContainingOrderByNameAsc(String name);

    @Query("select count(t) from ABranch b join Table t on b = t.branch where b = :branch")
    int getTableCountForABranch(@Param("branch") ABranch branch);

    @Query("SELECT eb.employeeBranchID.branch FROM Cook c JOIN EmployeeBranch eb ON c = eb.employeeBranchID.employee where :dish member of c.dishList ORDER BY eb.employeeBranchID.branch.name")
    List<ABranch> getBranchesByDish(@Param("dish")Dish dish);

    @Query("SELECT otd.orderTableDishID.table " +
            "FROM OrderTableDish otd " +
            "WHERE otd.orderTableDishID.table.branch = :branch " +
            "GROUP BY otd.orderTableDishID.table.id " +
            "HAVING SUM(otd.orderTableDishID.dish.price) >= ALL " +
            "(SELECT SUM(otd2.orderTableDishID.dish.price) " +
            "FROM OrderTableDish otd2 " +
            "WHERE otd2.orderTableDishID.table.branch = :branch " +
            "GROUP BY otd2.orderTableDishID.table.id)")
    List<Table> getMaxSalesTableByBranch(@Param("branch") ABranch branch);
}
