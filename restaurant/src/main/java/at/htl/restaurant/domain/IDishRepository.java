package at.htl.restaurant.domain;

import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IDishRepository extends JpaRepository<Dish,Long> {
    @Query("select di.dishIngredientID.dish from DishIngredient di where di.dishIngredientID.ingredient = :ing")
    List<Dish> getDishesByIngredient(@Param("ing")Ingredient ing);

    @Query("select count(otd.orderTableDishID.order) from OrderTableDish otd where otd.orderTableDishID.dish = :dish")
    int getOrderCount(@Param("dish") Dish dish);

    @Query("select otd.orderTableDishID.dish from OrderTableDish otd group by otd.orderTableDishID.dish having count(otd.orderTableDishID.dish) >= all (select count(otd2.orderTableDishID.dish) from OrderTableDish otd2 group by otd2.orderTableDishID.dish)")
    Dish getMaxOrderedDish();
}
