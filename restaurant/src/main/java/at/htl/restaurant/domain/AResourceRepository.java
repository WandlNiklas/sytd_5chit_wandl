package at.htl.restaurant.domain;

import at.htl.restaurant.model.AEntity;
import at.htl.restaurant.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface AResourceRepository<T,ID extends Serializable> extends JpaRepository<T,ID> {
}
