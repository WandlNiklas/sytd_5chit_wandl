package at.htl.restaurant.domain;

import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IIngredientRepository extends JpaRepository<Ingredient,Long> {
}
