package at.htl.restaurant.domain;

import at.htl.restaurant.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrderRepository extends JpaRepository<Order,Long> {
}
