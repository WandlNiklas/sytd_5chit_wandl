package at.htl.restaurant.domain;

import at.htl.restaurant.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IEmployeeRepository extends JpaRepository<Employee,Long> {
    public Employee readEmployeeById(Long id);
    public Employee readEmployeeBySocialSecurityCode(String socialSecurityCode);
    public List<Employee> readEmployeesByLastNameOrderByLastNameAsc(String lastName);
    public List<Employee> readEmployeesByLastNameContains(String token);
    @Query("select c from Employee c where type(c)=Cook order by c.lastName asc")
    List<Employee> getCooks();
    @Query("select wt.waiterTableID.table from WaiterTable wt WHERE wt.waiterTableID.waiter.lastName= :lastName")
    List<Table> getWaiterTables(@Param("lastName") String lastName);
    @Query("select eb.employeeBranchID.employee from EmployeeBranch eb join eb.employeeBranchID.employee e WHERE eb.employeeBranchID.branch.id = :branchId AND type(e) = Waiter")
    List<Waiter> getWaitersByABranchID(@Param("branchId") Long id);
    @Query("select c from Cook c where :dish member of c.dishList")
    List<Cook> getCooksByDish(@Param("dish") Dish dish);
}
