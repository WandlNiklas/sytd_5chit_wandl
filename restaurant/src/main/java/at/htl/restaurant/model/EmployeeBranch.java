package at.htl.restaurant.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name="FILIALEN_ANGESTELLTE")
public class EmployeeBranch implements Serializable {
    @EmbeddedId
    private EmployeeBranchID employeeBranchID;
}
