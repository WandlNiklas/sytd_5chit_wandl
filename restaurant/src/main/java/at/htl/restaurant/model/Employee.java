package at.htl.restaurant.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
@Entity
@Data
@Component
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json-type") //json neues Attribut
@JsonSubTypes({
        @JsonSubTypes.Type(value = Waiter.class, name = "waiter"),
        @JsonSubTypes.Type(value = Cook.class, name = "cook")})
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="ANGESTELLTE")
public class Employee extends AEntity {
    @NotNull
    @Column(name = "VORNAME",length=50,nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "NACHNAME",length=50,nullable = false)
    private String lastName;

    @NotNull
    @Column(name="SOZIALVERSICHERUNGS_NR",length = 10,nullable = false)
    private String socialSecurityCode;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;
}
