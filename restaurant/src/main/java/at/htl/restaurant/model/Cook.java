package at.htl.restaurant.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
@Data
@Table(name="KOECHE")
@Entity
public class Cook extends Employee {

    @ManyToMany
    @JoinTable(name = "KOECHE_GERICHTE")
    private List<Dish> dishList = new ArrayList<>();

}
