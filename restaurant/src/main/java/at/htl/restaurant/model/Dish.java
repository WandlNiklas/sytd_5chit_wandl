package at.htl.restaurant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
@Entity
@Table(name="GERICHTE")
public class Dish extends AEntity {
    @NotNull
    @Column(name="BEZEICHNUNG",nullable = false,unique = true,length=50)
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name="ART",nullable = false,length=20)
    private EKind kind;

    @Min(0)
    @NotNull
    @Column(name="PREIS",nullable = false)
    private Integer price;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;
}
