package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.List;
@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class WaiterTableID implements Serializable {
    @ManyToOne
    @JoinColumn(name = "WAITER_ID")
    private Waiter waiter;

    @ManyToOne
    @JoinColumn(name = "TABLE_LIST_ID")
    private Table table;
}
