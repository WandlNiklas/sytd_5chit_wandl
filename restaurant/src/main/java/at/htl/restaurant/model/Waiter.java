package at.htl.restaurant.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
@Data
@Table(name="KELLNER")
@Entity
public class Waiter extends Employee {

    @NotNull
    @Column(name="IST_UMSATZBETEILIGT",nullable = false)
    private Boolean earnsPart;
}
