package at.htl.restaurant.model;

import javax.persistence.*;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="SESSEL")
public class Chair extends AEntity {
    @Size(max = 3)
    @NotNull
    @Column(name="CODE",length=3,nullable = false,unique = true)
    private String code;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;

    @ManyToOne
    @JoinColumn(name = "TABLE_ID")
    private at.htl.restaurant.model.Table table;
}
