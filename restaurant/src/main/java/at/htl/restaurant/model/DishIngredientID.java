package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class DishIngredientID implements Serializable {
    @ManyToOne
    @JoinColumn(name = "GERICHT_ID")
    private Dish dish;

    @ManyToOne
    @JoinColumn(name = "ZUTAT_ID")
    private Ingredient ingredient;
}
