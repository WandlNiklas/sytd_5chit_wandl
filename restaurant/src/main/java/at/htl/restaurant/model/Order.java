package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Time;
@Data
@Entity
@Table(name="BESTELLUNGEN")
public class Order extends AEntity {
    @Column(name="BESTELLUNGS_ID",length = 11,unique = true)
    private Integer orderId;

    @Column(name="BESTELLUNGS_ZEITPUNKT")
    private Time orderTime;

    @Column(name="VERLAUF",length=255)
    private String history;
    
    @Column(name="VERSION")
    private Long version;
}
