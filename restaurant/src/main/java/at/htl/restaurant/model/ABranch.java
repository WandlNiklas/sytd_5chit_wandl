package at.htl.restaurant.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json-type") //json neues Attribut
@JsonSubTypes({
        @JsonSubTypes.Type(value = Branch.class, name = "branch"),
        @JsonSubTypes.Type(value = GuestGardenBranch.class, name = "guestGardenBranch")})
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name="A_FILIALEN")
@Entity
@Data
public abstract class ABranch extends AEntity
{
    @NotNull
    @Length(max = 50)
    @Column(name = "NAME", length = 50, unique = true, nullable = false)
    private String name;

    @NotNull
    @Length(max=200)
    @Column(name = "ADRESSE", length = 200, nullable = false)
    private String address;

    @NotNull
    @Length(max=30)
    @Column(name = "BEZIRK", length=30,nullable = false)
    private String district;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;
}
