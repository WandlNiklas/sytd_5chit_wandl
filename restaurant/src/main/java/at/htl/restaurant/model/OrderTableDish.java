package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
@Entity
@Table(name="BESTELLUNGEN_TISCHE_GERICHTE")
public class OrderTableDish implements Serializable {
    @EmbeddedId
    private OrderTableDishID orderTableDishID;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;
}
