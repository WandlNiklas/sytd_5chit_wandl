package at.htl.restaurant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@javax.persistence.Table(name = "TISCHE")
public class Table extends AEntity {

    @NotNull
    @Column(name = "IST_RAUCHER_TISCH",nullable = false)
    private Boolean isSmokerTable;

    @Min(0)
    @NotNull
    @Column(name ="TISCH_NR",nullable = false)
    private Integer tableNumber;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "FILIALE_ID",nullable = false)
    private ABranch branch;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;
}
