package at.htl.restaurant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ZUTATEN")
public class Ingredient extends AEntity {
    @NotNull
    @Column(name="BEZEICHNUNG",nullable = false,unique = true,length=50)
    private String description;

    @NotNull
    @Min(0)
    @Column(name="LAGERBESTAND",nullable = false)
    private Integer count;

    @NotNull
    @Column(name="VERSION",nullable = false)
    private Long version;
}
