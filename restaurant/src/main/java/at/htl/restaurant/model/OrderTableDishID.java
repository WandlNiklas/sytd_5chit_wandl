package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class OrderTableDishID implements Serializable {
    @ManyToOne
    @JoinColumn(name = "BESTELLUNGS_ID")
    private Order order;

    @ManyToOne
    @JoinColumn(name = "TISCH_ID")
    private Table table;

    @ManyToOne
    @JoinColumn(name = "GERICHT_ID")
    private Dish dish;
}
