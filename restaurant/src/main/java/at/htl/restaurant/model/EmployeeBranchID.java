package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeBranchID implements Serializable {
    @ManyToOne
    @JoinColumn(name = "AEMPLOYEE_ID")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "BRANCH_LIST_ID")
    private ABranch branch;
}
