package at.htl.restaurant.model;

import javax.persistence.*;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name="FILIALEN_GASTGARTEN")
@Entity
public class GuestGardenBranch extends ABranch {
    @Temporal(TemporalType.TIME)
    @NotNull
    @Column(name="OEFFNUNGS_ZEIT",nullable = false)
    private Date openingTime;

    @Temporal(TemporalType.TIME)
    @NotNull
    @Column(name="SCHLUSS_ZEIT",nullable = false)
    private Date closingTime;
}
