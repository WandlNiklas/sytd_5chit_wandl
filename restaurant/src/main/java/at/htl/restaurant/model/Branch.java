package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.*;
import javax.persistence.Table;

@Table(name="FILIALEN")
@Entity
@Data
public class Branch extends ABranch {
}
