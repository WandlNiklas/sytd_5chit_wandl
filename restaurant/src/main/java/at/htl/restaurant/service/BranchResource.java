package at.htl.restaurant.service;

import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.model.ABranch;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.transaction.Transactional;
import java.awt.*;

@RequestMapping(path="/branches")
@RestController //macht aus Klasse HTTP/REST Service
public class BranchResource {
    private static Logger log = LoggerFactory.getLogger(BranchResource.class);

    @GetMapping(path="/greet",produces=MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String greeting(){
        log.info("greeting was issued");
        return "Grüß dir Naul Pagl";
    }

    @Autowired
    private IABranchRepository branchRepository;
    //POST -> create in rest //PUT -> update in rest //in http genau umgekehrt
    @Transactional
    @GetMapping(path="/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE) //welches Format Json,Xml,... Resultat der Anfrage -> produziert durch ID Json Antwort (Branch)
    @ResponseStatus(HttpStatus.OK)
    public ABranch read(@PathVariable("id")Long id){
        ABranch branch = branchRepository.getOne(id);
        log.info("loaded branch with id:"+branch.toString());

        return (ABranch) Hibernate.unproxy(branch);
    }

    @Transactional //Transaktion wird gestartet, wenn alles durchgelaufen ist, wird diese beendet
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ABranch create(@RequestBody ABranch branch){ //im Request-Body stehen die notwendigen Daten
        branch = branchRepository.save(branch);
        log.info("create branch: "+ branch.getId());
        return (ABranch) Hibernate.unproxy(branch);
    }

    @Transactional
    @PutMapping(path="/update/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable("id")Long id, @RequestBody ABranch branch){
        branch = branchRepository.save(branch);
        log.info("update branch: "+ id);
    }

    @Transactional
    @DeleteMapping(path = "/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id")Long id)
    {
        branchRepository.deleteById(id);
        log.info("deleted branch: "+ id);
    }
}
