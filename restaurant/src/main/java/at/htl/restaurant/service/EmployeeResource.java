package at.htl.restaurant.service;

import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Employee;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RequestMapping(path="/employees")
@RestController //macht aus Klasse HTTP/REST Service
public class EmployeeResource {
    private static Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    @Autowired
    private IEmployeeRepository employeeRepository;
    //POST -> create in rest //PUT -> update in rest //in http genau umgekehrt
    @Transactional
    @GetMapping(path="/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE) //welches Format Json,Xml,... Resultat der Anfrage -> produziert durch ID Json Antwort (Branch)
    @ResponseStatus(HttpStatus.OK)
    public Employee read(@PathVariable("id")Long id){
        Employee employee = employeeRepository.getOne(id);
        log.info("loaded employee with id:"+employee.toString());

        return (Employee) Hibernate.unproxy(employee);
    }

    @Transactional //Transaktion wird gestartet, wenn alles durchgelaufen ist, wird diese beendet
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Employee create(@RequestBody Employee employee){ //im Request-Body stehen die notwendigen Daten
        employee = employeeRepository.save(employee);
        log.info("create employee: "+ employee.getId());
        return (Employee) Hibernate.unproxy(employee);
    }

    @Transactional
    @PutMapping(path="/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable("id")Long id, @RequestBody Employee employee){
        employee = employeeRepository.save(employee);
        log.info("update employee: "+ id);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id")Long id)
    {
        employeeRepository.deleteById(id);
        log.info("deleted employee: "+ id);
    }
}
