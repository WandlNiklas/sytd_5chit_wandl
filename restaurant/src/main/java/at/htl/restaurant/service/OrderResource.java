package at.htl.restaurant.service;

import at.htl.restaurant.model.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path="/order")
@RestController //macht aus Klasse HTTP/REST Service
public class OrderResource extends AResource<Order,Long>{
    private static Logger log = LoggerFactory.getLogger(Order.class);

    public OrderResource(@Autowired JpaRepository<Order,Long> rep)
    {
       super(rep);
    }
}
