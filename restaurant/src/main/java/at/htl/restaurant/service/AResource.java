package at.htl.restaurant.service;

import at.htl.restaurant.model.AEntity;
import lombok.*;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.lang.invoke.MethodHandles;
@RequiredArgsConstructor
public abstract class AResource <T extends AEntity, ID extends Serializable>{
    private static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @NonNull
    public JpaRepository<T,ID> aResourceRepository;
    //POST -> create in rest //PUT -> update in rest //in http genau umgekehrt
    @Transactional
    @GetMapping(path="/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE) //welches Format Json,Xml,... Resultat der Anfrage -> produziert durch ID Json Antwort (Branch)
    @ResponseStatus(HttpStatus.OK)
    public T read(@PathVariable("id")ID id){
        T t = (T)aResourceRepository.getOne(id);
        log.info("loaded branch with id:"+t.toString());

        return (T) Hibernate.unproxy(t);
    }

    @Transactional //Transaktion wird gestartet, wenn alles durchgelaufen ist, wird diese beendet
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public T create(@RequestBody T t){ //im Request-Body stehen die notwendigen Daten
        t = (T)aResourceRepository.save(t);
        log.info("create branch: "+ t.getId());
        return (T) Hibernate.unproxy(t);
    }

    @Transactional
    @PutMapping(path="/{id}",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable("id")ID id, @RequestBody T t){
        t = (T)aResourceRepository.save(t);
        log.info("update branch: "+ id);
    }

    @Transactional
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id")ID id)
    {
        aResourceRepository.deleteById(id);
        log.info("deleted branch: "+ id);
    }
}
