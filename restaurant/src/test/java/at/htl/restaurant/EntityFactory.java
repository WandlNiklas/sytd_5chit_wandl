package at.htl.restaurant;

import at.htl.restaurant.model.*;

public class EntityFactory {
    public static ABranch createDefaultBranchData(){
        Branch branch = new Branch();
        branch.setName("Pizza Alcapone");
        branch.setDistrict("Tulln");
        branch.setAddress("A-3470 Kirchberg am Wagram, Marktplatz 3");
        branch.setVersion(new Long(1L));
        return branch;
    }
    public static Employee createDefaultEmployeeData(){
        Waiter waiter = new Waiter();
        waiter.setFirstName("Sebastian");
        waiter.setLastName("Ferchenbauer");
        waiter.setSocialSecurityCode("1337200100");
        waiter.setVersion(new Long(1L));
        waiter.setEarnsPart(false);
        return waiter;
    }
    public static Order createDefaultOrderData(){
        Order order = new Order();
        order.setHistory("bla");
        return order;
    }
}
