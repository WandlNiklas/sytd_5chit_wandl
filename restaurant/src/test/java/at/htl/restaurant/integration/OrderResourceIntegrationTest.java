package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.model.Order;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;

//Integrationstest testet Zusammenspiel der Komponenten und Services
@RunWith(SpringRunner.class) //integriert Klasse in MavenTests
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) //alle Services in der Anwendung werden gestartet
@ActiveProfiles("test")
public class OrderResourceIntegrationTest {
    private static Logger log = LoggerFactory.getLogger(OrderResourceIntegrationTest.class);

    @Autowired
    private JpaRepository<Order,Long> aResourceRepository;

    @Test
    @Transactional
    public void readOrder(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","order/1");

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Order> httpResponse = restClient.getForEntity(requestURL, Order.class);

        Order order = httpResponse.getBody();
        Order order2 = (Order)aResourceRepository.getOne(1L);
        assertEquals(order2.getId(),order.getId());
    }
    @Test
    @Transactional
    public void createOrder(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","order");

        Order order = EntityFactory.createDefaultOrderData();
        HttpEntity<Order> httpData = new HttpEntity<>(order);
        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Order> httpResponse = restClient.postForEntity(requestURL,httpData,Order.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
        Order createOrder = httpResponse.getBody();
        assertNotNull(createOrder.getId());
    }
    @Test
    @Transactional
    public void updateOrder(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","order/1");

        Order order = (Order) Hibernate.unproxy(aResourceRepository.getOne(1L));
        order.setHistory("updated bla");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Order> httpData = new HttpEntity<>(order,httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT,httpData, Void.class);
        Order updatedEmployee = aResourceRepository.getOne(1L);

        assertEquals("updated bla", updatedEmployee.getHistory());
    }
    @Test
    public void deleteOrder(){
        Order order = EntityFactory.createDefaultOrderData();
        order = aResourceRepository.save(order);
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","order",order.getId());
        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);

        Optional<Order> delOrder = aResourceRepository.findById(order.getId());
        assertFalse(delOrder.isPresent());
    }
}
