package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.model.ABranch;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;
//Integrationstest testet Zusammenspiel der Komponenten und Services
@RunWith(SpringRunner.class) //integriert Klasse in MavenTests
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) //alle Services in der Anwendung werden gestartet
@ActiveProfiles("test")
public class BranchResourceIntegrationTest {
    private static Logger log = LoggerFactory.getLogger(BranchResourceIntegrationTest.class);

    @Autowired
    private IABranchRepository branchRepository;

    @Test
    public void greeting(){
        log.info("Grüß dir Paul");
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","branches/greet");

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<String> httpResponse = restClient.getForEntity(requestURL,String.class);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);

        String msg = httpResponse.getBody();
        assertEquals("Grüß dir Naul Pagl",msg);
    }
    @Test
    @Transactional
    public void readBranch(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","branches/1");

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<ABranch> httpResponse = restClient.getForEntity(requestURL, ABranch.class);

        ABranch branch = httpResponse.getBody();
        assertEquals(branchRepository.getOne(1L).getId(),branch.getId());
    }
    @Test
    @Transactional
    public void createBranch(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","branches");

        ABranch branch = EntityFactory.createDefaultBranchData();
        HttpEntity<ABranch> httpData = new HttpEntity<>(branch);
        RestTemplate restClient = new RestTemplate();
        ResponseEntity<ABranch> httpResponse = restClient.postForEntity(requestURL,httpData,ABranch.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
        ABranch createdBranch = httpResponse.getBody();
        assertNotNull(createdBranch.getId());
    }
    @Test
    @Transactional
    public void updateBranch(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","branches/update/1");

        ABranch branch = (ABranch) Hibernate.unproxy(branchRepository.getOne(1L));
        branch.setName("updated branch");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<ABranch> httpData = new HttpEntity<>(branch,httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT,httpData, Void.class);
        ABranch updatedBranch = branchRepository.getOne(1L);

        assertEquals("updated branch", updatedBranch.getName());
    }
    @Test
    public void deleteBranch(){
        ABranch branch = EntityFactory.createDefaultBranchData();
        branch = branchRepository.save(branch);
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","branches/delete",branch.getId());
        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);

        Optional<ABranch> delBranch = branchRepository.findById(branch.getId());
        assertFalse(delBranch.isPresent());
    }
}
