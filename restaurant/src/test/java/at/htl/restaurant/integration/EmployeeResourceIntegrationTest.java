package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Waiter;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;

//Integrationstest testet Zusammenspiel der Komponenten und Services
@RunWith(SpringRunner.class) //integriert Klasse in MavenTests
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) //alle Services in der Anwendung werden gestartet
@ActiveProfiles("test")
public class EmployeeResourceIntegrationTest {
    private static Logger log = LoggerFactory.getLogger(EmployeeResourceIntegrationTest.class);

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Test
    @Transactional
    public void readEmployee(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","employees/1");

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Employee> httpResponse = restClient.getForEntity(requestURL, Employee.class);

        Employee employee = httpResponse.getBody();
        assertEquals(employeeRepository.getOne(1L).getId(),employee.getId());
    }
    @Test
    @Transactional
    public void createEmployee(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","employees");

        Employee employee = EntityFactory.createDefaultEmployeeData();
        HttpEntity<Employee> httpData = new HttpEntity<>(employee);
        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Employee> httpResponse = restClient.postForEntity(requestURL,httpData,Employee.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());
        Employee createdEmployee = httpResponse.getBody();
        assertNotNull(createdEmployee.getId());
    }
    @Test
    @Transactional
    public void updateEmployee(){
        String requestURL = String.format("%s/%s/%s","http://127.0.0.1:8181","restaurant","employees/1");

        Employee employee = (Employee) Hibernate.unproxy(employeeRepository.getOne(1L));
        employee.setFirstName("updated employee");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Employee> httpData = new HttpEntity<>(employee,httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT,httpData, Void.class);
        Employee updatedEmployee = employeeRepository.getOne(1L);

        assertEquals("updated employee", updatedEmployee.getFirstName());
    }
    @Test
    public void deleteEmployee(){
        Employee employee = EntityFactory.createDefaultEmployeeData();
        employee = employeeRepository.save(employee);
        String requestURL = String.format("%s/%s/%s/%d","http://127.0.0.1:8181","restaurant","employees",employee.getId());
        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);

        Optional<Employee> delEmployee = employeeRepository.findById(employee.getId());
        assertFalse(delEmployee.isPresent());
    }
}
