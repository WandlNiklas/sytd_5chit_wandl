package at.htl.restaurant.unit;

import at.htl.restaurant.model.Cook;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Waiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class EmployeeFactory {

    @Autowired
    private IDGenerator generator;

    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    @Bean("cook") @Qualifier("cook")
    public Employee createDefaultCook() {
        Cook cook = new Cook();
        cook.setLastName("Nagelmaier");
        cook.setFirstName("Jonas");
        cook.setSocialSecurityCode(generator.generateSozialSecurityCode());
        return cook;
    }

    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    @Bean("waiter") @Qualifier("waiter")
    public Employee createDefaultWaiter() {
        Waiter waiter = new Waiter();
        waiter.setLastName("Schandl");
        waiter.setFirstName("Lukas");
        waiter.setSocialSecurityCode(generator.generateSozialSecurityCode());
        return waiter;
    }
}
