package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IABranchRepository;
import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.ABranch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Table;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BranchRepositoryUnitTests {

    private static Logger LOG = LoggerFactory.getLogger(BranchRepositoryUnitTests.class);

    @Autowired
    private IABranchRepository branchRepository;

    @Autowired
    private IDishRepository dishRepository;

    @Transactional
    @Test
    public void getBranchByNameToken(){
        List<ABranch> branches = branchRepository.getABranchByNameContainingOrderByNameAsc("m");
        assertEquals(5,branches.size());
    }

    @Transactional
    @Test
    public void getTableCountForABranch(){
        ABranch branch = branchRepository.getOne(1L);
        int tableCount = branchRepository.getTableCountForABranch(branch);
        assertEquals(4,tableCount);
    }
    @Transactional
    @Test
    public void getBranchesByDish(){
        Dish dish = dishRepository.getOne(1L);
        List<ABranch> branches = branchRepository.getBranchesByDish(dish);
        assertEquals(1,branches.size());
    }
    @Transactional
    @Test
    public void getMaxSalesTableByBranch(){
        ABranch branch = branchRepository.getOne(1L);
        List<Table> tables = branchRepository.getMaxSalesTableByBranch(branch);
        assertEquals(2,(long)tables.get(0).getId());
    }
}
