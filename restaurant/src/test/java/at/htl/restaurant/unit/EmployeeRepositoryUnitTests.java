package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeRepositoryUnitTests {

    private static Logger LOG = LoggerFactory.getLogger(EmployeeRepositoryUnitTests.class);

    @Test
    public void testHelloWorld(){
        LOG.info("hallo welt");
    }

    @Autowired //Dependency Injection, automatische Zuweisung, Lebenszyklus wird vom System verwaltet
    private IEmployeeRepository employeeRepository;

    @Autowired
    private IDishRepository dishRepository;

    @Transactional
    @Test
    public void readEmployeeById(){
        Employee employee = employeeRepository.readEmployeeById(1l);
        assertEquals(new Long(1l),employee.getId());
    }

    @Transactional
    @Test
    public void readEmployeeBySocialSecurityCode(){
        Employee employee = employeeRepository.readEmployeeBySocialSecurityCode("17091999");
        assertEquals("Draeger",employee.getLastName());
    }

    @Transactional
    @Test
    public void readEmployeesByLastNameOrderByLastName(){
        List<Employee> employees = employeeRepository.readEmployeesByLastNameOrderByLastNameAsc("Leister");
        assertEquals(1,employees.size());
    }
    @Transactional
    @Test
    public void readEmployeesByLastNameContains(){
        List<Employee> employees = employeeRepository.readEmployeesByLastNameContains("K");
        assertEquals(3,employees.size());
    }
    @Transactional
    @Test
    public void readAllCooks(){
        List<Employee> cooks = employeeRepository.getCooks();
        assertEquals(7,cooks.size());
    }
    @Transactional
    @Test
    public void readSpecificWaiterTables(){
        List<Table> tables = employeeRepository.getWaiterTables("Kropik");
        assertEquals(4,tables.size());
    }
    @Transactional
    @Test
    public void getWaitersByABranchID(){
        List<Waiter> waiters = employeeRepository.getWaitersByABranchID(1L);
        assertEquals(3,waiters.size());
    }
    @Transactional
    @Test
    public void getCooksByDish(){
        Dish dish = dishRepository.getOne(1L);
        List<Cook> cooks = employeeRepository.getCooksByDish(dish);
        assertEquals(2,cooks.size());
    }
}
