package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BranchUnitTests {

    private static Logger LOG = LoggerFactory.getLogger(BranchUnitTests.class);

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Transactional
    @Test
    public void readEmployeeById(){
        Employee employee = employeeRepository.readEmployeeById(1l);
        assertEquals(new Long(1l),employee.getId());
    }

    @Transactional
    @Test
    public void readEmployeeBySocialSecurityCode(){
        Employee employee = employeeRepository.readEmployeeBySocialSecurityCode("17091999");
        assertEquals("Draeger",employee.getLastName());
    }

    @Transactional
    @Test
    public void readEmployeesByLastNameOrderByLastName(){
        List<Employee> employees = employeeRepository.readEmployeesByLastNameOrderByLastNameAsc("Leister");
        assertEquals(1,employees.size());
    }
    @Transactional
    @Test
    public void readEmployeesByLastNameContains(){
        List<Employee> employees = employeeRepository.readEmployeesByLastNameContains("K");
        assertEquals(3,employees.size());
    }
}
