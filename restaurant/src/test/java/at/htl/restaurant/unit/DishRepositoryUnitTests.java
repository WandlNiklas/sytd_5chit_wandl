package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.domain.IIngredientRepository;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Ingredient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DishRepositoryUnitTests {

    private static Logger LOG = LoggerFactory.getLogger(DishRepositoryUnitTests.class);

    @Autowired
    private IDishRepository dishRepository;

    @Autowired
    private IIngredientRepository ingredientRepository;

    @Transactional
    @Test
    public void getDishesByIngredient(){
        Ingredient ingredient = ingredientRepository.getOne(1L);
        List<Dish> dishes = dishRepository.getDishesByIngredient(ingredient);
        assertEquals(4,dishes.size());
    }
    @Transactional
    @Test
    public void getOrderCount(){
        Dish dish = dishRepository.getOne(1L);
        int orderCount = dishRepository.getOrderCount(dish);
        assertEquals(1,orderCount);
    }
    @Transactional
    @Test
    public void getMaxOrderedDish(){
        Dish maxOrderedDish = dishRepository.getOne(11L);
        Dish dish = dishRepository.getMaxOrderedDish();
        assertEquals(maxOrderedDish,dish);
    }
}
