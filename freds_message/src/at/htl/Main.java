package at.htl;

import java.util.concurrent.CountDownLatch;

public class Main {

    public static void main(String[] args) {
        Thread producer = new Thread(new Producer());
        producer.start();

        CountDownLatch latch = new CountDownLatch(3);

        for (int i = 0; i < 3; i++) {
            Thread consumer = new Thread(new Consumer(latch,i+1));
            consumer.start();
            latch.countDown();
        }
    }
}
