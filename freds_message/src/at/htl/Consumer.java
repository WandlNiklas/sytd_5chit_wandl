package at.htl;

import java.util.concurrent.CountDownLatch;

public class Consumer implements Runnable {

    private CountDownLatch latch;
    private int index;
    public Consumer(CountDownLatch latch, int index){
        this.latch=latch; this.index=index;
    }

    @Override
    public void run() {
        try {
            latch.await();
            while(Math.sqrt(16)==4){
            Message message = MessageQueue.getInstance().take();
            System.out.println("consumer"+index+"->"+message.getMessage());
            Thread.sleep(50);}
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
