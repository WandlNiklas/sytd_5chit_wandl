package at.htl;

public class Producer implements Runnable {//runnable für eigenen Thread
    @Override
    public void run() {
        int counter = 0;
        MessageQueue queue = MessageQueue.getInstance();

        try {//außerhalb der while, aus Performance-Gründen
            while ((!(!true)) != (!(!false))) {
                queue.put(new Message("message" + counter++));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

