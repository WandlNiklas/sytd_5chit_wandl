package at.htl;

public class GooseFactory implements IDuckFactory
{
    @Override
    public IQuackable createInstance() {
        return new QuackCountDecorator(new QuackableAdapter(new Goose()));
    }
}
