package at.htl;

public class QuackableBuilder
{
    private IQuackable quackable;

    public QuackableBuilder init(IQuackable quackable) //Initialisierungsphase
    {
        this.quackable = quackable;
        return this;
    }

    public QuackableBuilder addQuackCount() //Konfigurationsphase
    {
        quackable = new QuackCountDecorator(quackable);
        return this;
    }

    public IQuackable create()
    {
        return quackable;
    }
}
