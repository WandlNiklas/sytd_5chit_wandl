package at.htl;

public class RubberDuckFactory implements IDuckFactory
{
    @Override
    public IQuackable createInstance() {
        return new QuackCountDecorator(new RubberDuck());
    }
}
