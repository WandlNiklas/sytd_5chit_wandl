package at.htl;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class QuackCountDecorator implements IQuackable {

    IQuackable quackable;

    @Getter
    private static Integer counter = 0;

    @Override
    public void quack(){
        quackable.quack();
        counter++;
    }
}
