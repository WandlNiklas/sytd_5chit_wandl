package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class QuackableAdapter implements IQuackable {

    private IHonkable honkable;
    @Override
    public void quack(){
        honkable.honk();
    }
}
