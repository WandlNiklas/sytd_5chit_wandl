package at.htl;

public class Main {

    public static void main(String[] args) {
		IQuackable rubberDuck = new QuackCountDecorator(new RubberDuck());
		IQuackable mullardDuck = new QuackCountDecorator(new MullardDuck());
		IQuackable redHeadDuck = new QuackCountDecorator(new RedHeadDuck());
		IQuackable goose = new QuackableAdapter(new Goose());

		DuckSimulator duckSimulator = new DuckSimulator();
		duckSimulator.simulate(rubberDuck);
		duckSimulator.simulate(mullardDuck);
		duckSimulator.simulate(redHeadDuck);
		duckSimulator.simulate(goose);

		//FACTORY
		GooseFactory gf = new GooseFactory();
		RubberDuckFactory rf = new RubberDuckFactory();

		IQuackable goose2 = gf.createInstance();
		IQuackable rubberDuckk = rf.createInstance();

		duckSimulator.simulate(goose2);
		duckSimulator.simulate(rubberDuckk);

		//BUILDER
		QuackableBuilder quackBuilder = new QuackableBuilder();

		IQuackable quackable = quackBuilder.init(new RubberDuck()).addQuackCount().create();
		quackable.quack();

		goose2 = quackBuilder.init(new QuackableAdapter(new Goose())).addQuackCount().create();
		goose2.quack();
	}
}
