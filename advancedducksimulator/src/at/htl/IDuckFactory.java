package at.htl;

public interface IDuckFactory {
    IQuackable createInstance();
}
