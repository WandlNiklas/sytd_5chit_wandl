package at.htl;

public class DuckSimulator {
    public void simulate(IQuackable quackable){
        quackable.quack();
    }
}
