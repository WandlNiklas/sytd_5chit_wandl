package at.htl;

import java.util.concurrent.Semaphore;

public class Counter {
    //int simple -> Stack Integer Referenz -> Heap
    private int value = 0;

    private Semaphore sem;

    public Counter(Semaphore sem){
        this.sem = sem;
    }

    public int IncrementAndGet() throws InterruptedException {
        sem.acquire();
        value++;
        Thread.sleep(2);
        sem.release();
        return value;
    }
}
