package at.htl;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ADuck implements Serializable {
    private IQuackBehaviour quackBehaviour;
    private IFlyBehaviour flyBehaviour;
}
