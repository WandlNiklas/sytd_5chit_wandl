package at.htl.messaging2019.integration;

import at.htl.messaging2019.message.producer.ArticleProducer;
import at.htl.messaging2019.model.Article;
import at.htl.messaging2019.service.WebshopResource;
import com.sun.xml.internal.bind.v2.util.QNameMap;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class) //integriert Klasse in MavenTests
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) //alle Services in der Anwendung werden gestartet
@ActiveProfiles("test")
public class WebShopIntegrationTests {
    private static Logger log = LoggerFactory.getLogger(WebShopIntegrationTests.class);

    @Autowired
    private WebshopResource webshopResource;

    @Test
    @Transactional
    public void read() {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "messaging", "direct");

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Article> httpResponse = restClient.getForEntity(requestURL, Article.class);

        Article article = httpResponse.getBody();
        QNameMap<Object> branchRepository;
        log.info(article.getName());
    }
}