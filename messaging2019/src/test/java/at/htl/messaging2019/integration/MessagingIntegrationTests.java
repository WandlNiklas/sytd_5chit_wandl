package at.htl.messaging2019.integration;

import at.htl.messaging2019.model.Article;
import at.htl.messaging2019.model.Employee;
import at.htl.messaging2019.service.MessagingResource;
import at.htl.messaging2019.service.WebshopResource;
import com.sun.xml.internal.bind.v2.util.QNameMap;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class) //integriert Klasse in MavenTests
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) //alle Services in der Anwendung werden gestartet
@ActiveProfiles("test")
public class MessagingIntegrationTests {
    private static Logger log = LoggerFactory.getLogger(MessagingIntegrationTests.class);

    @Autowired
    private MessagingResource messagingResource;

    @Test
    public void readMessage() {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "messaging", "message/direct");

        RestTemplate restClient = new RestTemplate();
        Employee employee = new Employee("Hans","Herbert");
        HttpEntity<Employee> httpResponse = new HttpEntity<Employee>(employee);
        restClient.postForEntity(requestURL,httpResponse,Void.class);
        log.info(employee.getFname());
    }
}