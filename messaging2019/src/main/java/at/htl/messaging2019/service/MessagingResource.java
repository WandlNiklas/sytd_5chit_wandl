package at.htl.messaging2019.service;

import at.htl.messaging2019.message.producer.DirectRabbitMQProducer;
import at.htl.messaging2019.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path="/message")
@RestController
public class MessagingResource {
    private static final Logger log = LoggerFactory.getLogger(MessagingResource.class);

    @Autowired
    private DirectRabbitMQProducer directProducer;

    @GetMapping(path="/ping", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String ping(){
        log.info("ping");
        return "received ping";
    }

    @PostMapping(path="/direct")
    @ResponseStatus(HttpStatus.OK)
    public void sendMessageToDirectExchange(@RequestBody Employee emp) throws JsonProcessingException{
        log.info("sending message to direct exchange");
        directProducer.sendDirectMessage(new Employee("Hans","Herbert"));
    }
}
