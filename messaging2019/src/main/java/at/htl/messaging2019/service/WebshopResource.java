package at.htl.messaging2019.service;

import at.htl.messaging2019.message.producer.ArticleProducer;
import at.htl.messaging2019.message.producer.DirectRabbitMQProducer;
import at.htl.messaging2019.model.Article;
import at.htl.messaging2019.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RequestMapping(path="/webshop")
@RestController
public class WebshopResource {
    private static final Logger log = LoggerFactory.getLogger(WebshopResource.class);

    @Autowired
    private ArticleProducer articleProducer;

    @PostMapping(path="/order")
    @ResponseStatus(HttpStatus.OK)
    public void orderArticle() throws JsonProcessingException{
        log.info("order article");
        articleProducer.sendArticle(new Article("Qboard Bauplatte","Mual","Trockenausbau"));
    }
}
