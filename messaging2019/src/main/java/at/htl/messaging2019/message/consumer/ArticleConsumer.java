package at.htl.messaging2019.message.consumer;

import at.htl.messaging2019.model.Article;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleConsumer
{
    @Autowired
    private ObjectMapper objectMapper;
    @RabbitListener(queues = {"htl.webshop.fliesen","htl.webshop.baustoffe","htl.webshop.boeden"},concurrency = "3")
    public void listenArticle(String message) throws JsonProcessingException {
        Article article = objectMapper.readValue(message, Article.class);
    }
}
