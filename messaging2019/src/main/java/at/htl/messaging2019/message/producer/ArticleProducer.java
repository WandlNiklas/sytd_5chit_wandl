package at.htl.messaging2019.message.producer;

import at.htl.messaging2019.model.Article;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArticleProducer {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    public void sendArticle(Article article)
            throws JsonProcessingException {
        String message = objectMapper.writeValueAsString(article);
        rabbitTemplate.convertAndSend("htl.webshop.exchange","htl.webshop."+article.getCategory()+"."+article.getName(), message);
    }
}
