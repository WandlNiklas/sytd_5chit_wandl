package at.htl.messaging2019.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
@ToString
@AllArgsConstructor
@Data
public class Article implements Serializable {
    private String name;
    private String description;
    private String category;
}
