package at.htl.messaging2019.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
@ToString
@AllArgsConstructor
@Data
public class Employee implements Serializable {
    private String fname;
    private String lname;
}
