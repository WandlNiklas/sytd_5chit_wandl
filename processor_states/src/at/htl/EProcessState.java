package at.htl;

public enum EProcessState {
    READY, RUNNING, BLOCKED, FINISHED
}
