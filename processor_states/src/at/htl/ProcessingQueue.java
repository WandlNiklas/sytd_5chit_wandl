package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class ProcessingQueue extends ArrayBlockingQueue<Process> {
    private static ProcessingQueue instance = new ProcessingQueue();

    private ProcessingQueue() {
        super(10);
    }

    public static ProcessingQueue getInstance(){
        return instance;
    }
}
