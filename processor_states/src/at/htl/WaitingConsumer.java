package at.htl;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class WaitingConsumer implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {
                Process process = WaitingQueue.getInstance().take();
                Random r = new Random();
                if (r.nextInt(10) <= 6) {
                    ProcessingQueue.getInstance().put(process);
                    process.switchState(EProcessState.RUNNING);
                }
                else
                    WaitingQueue.getInstance().put(process);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
