package at.htl;

import java.util.Random;

public class AbortedConsumer implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {
                Process process = AbortedQueue.getInstance().take();
                Random r = new Random();
                if (r.nextInt(10) <= 4) {
                    WaitingQueue.getInstance().put(process);
                    process.switchState(EProcessState.READY);
                }
                else
                    AbortedQueue.getInstance().put(process);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
