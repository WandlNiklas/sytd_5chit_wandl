package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class WaitingQueue extends ArrayBlockingQueue<Process> {
    private static WaitingQueue instance = new WaitingQueue();

    private WaitingQueue() {
        super(10);
    }

    public static WaitingQueue getInstance(){
        return instance;
    }
}
