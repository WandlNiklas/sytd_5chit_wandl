package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class AbortedQueue extends ArrayBlockingQueue<Process> {
    private static AbortedQueue instance = new AbortedQueue();

    private AbortedQueue() {
        super(10);
    }

    public static AbortedQueue getInstance(){
        return instance;
    }
}
