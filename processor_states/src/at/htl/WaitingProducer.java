package at.htl;

public class WaitingProducer implements Runnable {
    @Override
    public void run() {
        int counter = 0;
        WaitingQueue queue = WaitingQueue.getInstance();

        try {
            while (counter<=10) {
                queue.put(new Process("Task: "+counter++));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
