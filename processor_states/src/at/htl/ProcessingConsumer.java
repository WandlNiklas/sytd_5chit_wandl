package at.htl;

import java.util.Random;

public class ProcessingConsumer implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {
                Process process = ProcessingQueue.getInstance().take();
                System.out.println(process.getProcessID()+": processing");
                Random r = new Random();
                int result = r.nextInt(10);
                if (result <= 4) {
                    ProcessingQueue.getInstance().remove(process);
                    process.switchState(EProcessState.FINISHED);
                } else if (result > 4 && result <= 6) {
                    AbortedQueue.getInstance().put(process);
                    process.switchState(EProcessState.BLOCKED);
                } else if (result > 6 && result <= 8) {
                    WaitingQueue.getInstance().put(process);
                    process.switchState(EProcessState.READY);
                }
                else
                    ProcessingQueue.getInstance().put(process);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
